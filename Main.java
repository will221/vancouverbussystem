package com.example.vancouverbuswithjavafx;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.VBox;
import javafx.geometry.Insets;

import java.util.Scanner;
import java.util.ArrayList;
import java.io.FileNotFoundException;

import java.io.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;


public class Main extends Application {

    Stage window;
    Scene scene;
    Button button;
    ComboBox<String> comboBox;

    public ArrayList<String> findShortestRoute(String start, String end) {
        ArrayList<String> errors = new ArrayList<String>();

        try {
            int firstStop;
            firstStop = Integer.parseInt(start);
            int secondStop;
            secondStop = Integer.parseInt(end);
            functionOne vBusSystem = new functionOne();
            try {
                vBusSystem.buildBusStopMatrix();
                return vBusSystem.shortestRoute(firstStop, secondStop);
            } catch (FileNotFoundException e) {
                errors.add("Failed Building Bus Matrix");
                return errors;
            }

        } catch (NumberFormatException e) {
            errors.add("Invalid stops.");
            return errors;
        }
    }

    public ArrayList<String> findStopData(String inputtedStop) {
        ReadStopDataFromFile keyValueFile;
        keyValueFile = new ReadStopDataFromFile();
        TST tst;
        tst = new TST();
        File stopInfo;
        stopInfo = new File("stops.txt");

        ArrayList<String> keyArrayList;
        keyArrayList = keyValueFile.makeKeyArrayList(stopInfo);
        ArrayList<String> valueArrayList;
        valueArrayList = keyValueFile.makeValArrayList(stopInfo);

        for (AtomicInteger j = new AtomicInteger(); j.get() < keyArrayList.size(); j.getAndIncrement()) {
            String nowKey;
            nowKey = keyArrayList.get(j.get());

            String nowVal;
            nowVal = valueArrayList.get(j.get());

            tst.put(nowVal, nowKey);

        }
        ArrayList<String> result = tst.valuesWithPrefix(inputtedStop);
        return result;

    }




    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        window = primaryStage;
        window.setTitle(" Vancouver Bus System ");
        button = new Button("Submit");



        comboBox = new ComboBox<>();
        comboBox.getItems().addAll(
                "1. Find the Shortest Route Between Two Stops",
                "2. Enter Stop Name to Get Stop Information",
                "3. Enter time to find all trips departing at that time"
        );

        comboBox.setPromptText("How Can We Help?");
        button.setOnAction(e -> nextAction());


        VBox layout = new VBox(10);
        layout.setPadding(new Insets(10, 10, 10, 10));
        layout.getChildren().addAll(comboBox, button);

        scene = new Scene(layout, 300, 250);
        window.setScene(scene);
        window.show();


    }

    public void nextAction() {
        String chosenService = comboBox.getValue();
        Scanner sc = new Scanner(System.in);

        switch (chosenService) {
            case "1. Find the Shortest Route Between Two Stops" :
                System.out.println("Please Enter First Stop Name");
                String start = sc.nextLine();
                System.out.println("Please Enter Second Stop Name");
                String end = sc.nextLine();

                ArrayList<String> routeResult = findShortestRoute(start, end);
                String routeMessage = "";
                if(routeResult.isEmpty()) {
                    routeMessage = "No routes found, try again";
                } else {
                    for (String s : routeResult) {
                        routeMessage += s + "\n";
                    }
                }

                System.out.println(routeMessage);


                break;
            case "2. Enter Stop Name to Get Stop Information" :
                System.out.println("Please Search Stop Name");
                String stop = sc.nextLine();
                stop = stop.toUpperCase();
                System.out.println(stop);

                if (!stop.equals("")) {
                } else {
                    stop = "*";
                }

                ArrayList<String> stopData = findStopData(stop);

                String tstResult = "";
                if(stopData.isEmpty()) tstResult = "Stop doesn't exist. Try again!";
                else {

                    for (String s : stopData) tstResult += s + "\n";
                }

                System.out.println("Stops: " + tstResult);

                break;


            case "3. Enter time to find all trips departing at that time" :
                System.out.println("You Chose " + comboBox.getValue() + "/n" + "This service is currently under construction. Please return to the main menu and choose from one of our other services. ");

                break;



            default:
                System.out.println("Invalid Input, please try again!!");

        }

    }
}

class ReadStopDataFromFile {

    public static ArrayList<String> keyArrayList = new ArrayList<String>();
    public static ArrayList<String> valueArrayList = new ArrayList<String>();
    public static String[] keywords = { "FLAGSTOP", "SB", "WB", "NB", "EB" };


    public ArrayList<String> makeKeyArrayList(File file) {
        try {

            Scanner scan;
            scan = new Scanner(file);

            scan.nextLine();

            if (scan.hasNextLine()) {
                do {
                    String data = scan.nextLine().trim();
                    String[] key = data.split(",");
                    if (key[2].contains("WB ") ||
                            key[2].contains("FLAGSTOP ") ||
                            key[2].contains("NB ") ||
                            key[2].contains("SB ") ||
                            key[2].contains("EB ")) {
                        key[2] = (changeWordPos(key[2]));
                    }
                    keyArrayList.add(key[2]);
                } while (scan.hasNextLine());
            }
            scan.close();
        } catch (FileNotFoundException e) {
            System.out.println("Error");
            e.printStackTrace();
        }
        return keyArrayList;
    }


    public String changeWordPos(String str) {
        String[] element;
        element = str.split(" ");
        if (!Arrays.asList(keywords).contains(element[0])) {
            return str;
        } else {
            String temp = element[0];
            for (int counter = 1; counter < element.length; counter++) {
                element[counter - 1] = element[counter];
            }
            int length;
            length = element.length - 1;
            element[length] = temp;
            StringBuilder build = new StringBuilder();
            for (String s : element) {
                build.append(s);
                build.append(" ");
            }
            return build.toString();
        }
    }


    public ArrayList<String> makeValArrayList(File file) {
        try {

            Scanner sc;
            sc = new Scanner(file);
            sc.nextLine();
            if (sc.hasNextLine()) {
                do {
                    String data = sc.nextLine();
                    valueArrayList.add(data);
                } while (sc.hasNextLine());
            }
            sc.close();
        } catch (FileNotFoundException e) {
            System.out.println("Error");
            e.printStackTrace();
        }
        return valueArrayList;
    }
}
