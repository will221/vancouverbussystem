package com.example.vancouverbuswithjavafx;

import java.util.*;

public class TST {
    private int size; // size
    private Node<String> root; // root of TST

    private static class Node<String> {
        private char c; // Character
        private Node<String> left, mid, right; // left, mid and right subtrie
        private String val;
    }

    /**
     * @return the number of key-value pairs in this symbol table
     */
    public int size() {
        return size;
    }

    /**
     * @return true if key exists
     */
    public boolean contains(String key) {
        return get(key) != null;
    }

    /**
     * Returns the value associated with the given key.
     *
     * @param key the key
     * @return the value associated with the given key if the key is in the symbol
     *         table and {@code null} if the key is not in the symbol table
     * @throws IllegalArgumentException if {@code key} is {@code null} or empty
     */
    public String get(String key) {
        if (key == null) {
            throw new IllegalArgumentException("Key cannot be null");
        } else if (key.length() == 0) {
            throw new IllegalArgumentException("Key must be of length >= 1");
        }
        Node<String> node = get(root, key, 0);
        if(node == null) {
            return String.valueOf(node);
        }
        return node.val;
    }

    // Return subtrie corresponding to given key
    public Node<String> get(Node<String> node, String key, int depth) {

        if (node == null)
            return null;

        if (key.length() == 0) {
            throw new IllegalStateException("Key must be of length >= 1");
        }
        char c = key.charAt(depth);
        if (c < node.c)
            return get(node.left, key, depth);
        else if (c > node.c)
            return get(node.right, key, depth);
        else if (depth < key.length() - 1)
            return get(node.mid, key, depth + 1);

        else
            return node;
    }

    /**
     * Inserts the key-value pair into the symbol table, overwriting the old value
     * with the new value if the key is already in the symbol table. If the value is
     * {@code null}, this effectively deletes the key from the symbol table.
     *
     * @param key the key
     * @param val the value
     * @throws IllegalArgumentException if {@code key} is {@code null}
     */
    public void put(String val, String key) {
        if (val == null) {
            throw new IllegalArgumentException("Key cannot be null");
        }

        if (!contains(key)) {
            size++;
            root = put(root, key, val, 0);
        }
    }

    private Node<String> put(Node<String> node, String key, String val, int depth) {
        char c = key.charAt(depth);
        if (node == null) {
            node = new Node<>();
            node.c = c;
        }

        if (c < node.c)
            node.left = put(node.left, key, val, depth);
        else if (c > node.c)
            node.right = put(node.right, key, val, depth);
        else if (depth < key.length() - 1)
            node.mid = put(node.mid, key, val, depth + 1);
        else
            node.val = val;
        return node;
    }

    /**
     * Returns all of the keys in the set that start with {@code prefix}.
     *
     * @param prefix the prefix
     * @return all of the values in the set that start with {@code prefix}, as a
     *         list
     * @throws IllegalArgumentException if {@code prefix} is {@code null}
     */
    public ArrayList<String> valuesWithPrefix(String prefix) {

        if (prefix == null) {
            return new ArrayList<String>() {
                {
                    add("The string entered was null");
                }
            };
        }

        ArrayList<String> values = new ArrayList<>();

        Node<String> node = get(root, prefix, 0);

        if (node == null) {
            return values;
        } else if (node.val != null) {
            values.add(node.val);
        }
        collectSubTries(node.mid, new StringBuilder(prefix), values);
        if (values.get(0) == null) {
            return new ArrayList<String>() {
                {
                    add("The stop address was not recognised");
                }
            };
        }
        return values;
    }

    // All values in subtrie rooted at node with given prefix
    public void collectSubTries(Node<String> node, StringBuilder prefix, List<String> values) {
        if (node == null)
            return;
        collectSubTries(node.left, prefix, values);
        if (node.val != null)
            values.add(node.val);
        collectSubTries(node.mid, prefix.append(node.c), values);
        prefix.deleteCharAt(prefix.length() - 1);
        collectSubTries(node.right, prefix, values);
    }
}

// Code based on Robert Sedgewick and Kevin Wayne's TST.java
