package com.example.vancouverbuswithjavafx;

import java.io.*;
import java.util.*;

public class functionOne {

    public final int MAXIMUMN_NUMBER_OF_StopS = 12479;
    public double busStopMatrix[][] = new double[MAXIMUMN_NUMBER_OF_StopS][MAXIMUMN_NUMBER_OF_StopS];

    public functionOne() {
        try {
            buildBusStopMatrix();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void fillInTransferDetails(String fileName) throws FileNotFoundException {
        int start;
        int destination = 0;
        int lastRoute;
        int currRoute = 0;
        int transferType;
        double minTime;

        File transfersFile = new File(fileName);
        Scanner sc2 = null;
        Scanner sc = new Scanner(transfersFile);
        sc.nextLine();

        while(sc.hasNextLine()) {
            String currentLine = sc.nextLine();
            sc2 = new Scanner(currentLine);
            sc2.useDelimiter(",");
            start = sc2.nextInt();
            destination = sc2.nextInt();
            transferType = sc2.nextInt();

            if(transferType == 0) {
                busStopMatrix[start][destination] = 2;
            }

            else if(transferType == 2) {
                minTime = sc2.nextDouble();
                busStopMatrix[start][destination] = minTime / 100;
            }

            sc2.close();
        }
        sc.close();
    }

    public void buildBusStopMatrix() throws FileNotFoundException {
        int len = busStopMatrix.length;
        for(int i = 0; i < len; i++){
            busStopMatrix[i][i] = 0;
        }
        for(int i = 0; i < len; i++) {
            for(int j = 0; j < busStopMatrix[i].length; j++) {
                if(i != j) {
                    busStopMatrix[i][j] = Double.POSITIVE_INFINITY;
                }
            }
        }

        fillInBusStopMatrixWithInitialSetup("Stop_times.txt");
        fillInTransferDetails("transfers.txt");


    }

    private void fillInBusStopMatrixWithInitialSetup(String fileName) throws FileNotFoundException {
        File StopTimesFile = new File(fileName);

        Scanner sc = new Scanner(StopTimesFile);
        sc.useDelimiter(",");
        if(sc.hasNextLine())
            sc.nextLine();

        int start;
        int destination = 0;
        int lastRoute;
        int currRoute = 0;
        int transferType;
        double minTime;

        while(sc.hasNextLine()){
            lastRoute = currRoute;
            start = destination;
            currRoute = sc.nextInt();
            sc.next();
            sc.next();
            destination = sc.nextInt();

            if (lastRoute == currRoute){
                busStopMatrix[start][destination] = 1;
            }
            sc.nextLine();
        }
        sc.close();
    }

    public ArrayList<String> shortestRoute(int start, int end){
        ArrayList<String> result = new ArrayList<String>();
        int len = busStopMatrix.length;

        if(start == end) {
            result.add("cost: " + Double.toString(busStopMatrix[start][end]));
            result.add(" via route: ");
            result.add(Integer.toString(start));
            return  result;
        }

        boolean visited[] = new boolean[len];
        double distanceTo[] = new double[len];
        int connectedTo[] = new int[len];
        for(int i = 0; i < distanceTo.length; i++) {
            distanceTo[i] = Double.POSITIVE_INFINITY;
        }

        visited[start] = true;
        distanceTo[start] = 0;
        int currentNode = start;
        int visitedCounter = 0;
        while(visitedCounter < distanceTo.length)
        {
            for(int i = 0; i < busStopMatrix[currentNode].length; i++) {
                if(!Double.isInfinite(busStopMatrix[currentNode][i]) && visited[i] == false) {
                    getShorterRoute(distanceTo, connectedTo, currentNode, i);
                }
            }
            visited[currentNode] = true;

            double shortestDist = Integer.MAX_VALUE;
            for(int i = 0; i < distanceTo.length; i++) {
                if(visited[i] != true && shortestDist > distanceTo[i]) {
                    currentNode = i;
                    shortestDist = distanceTo[i];
                }
            }
            visitedCounter++;
        }

        if(distanceTo[end] == Double.POSITIVE_INFINITY) {
            result.add("No route found");
            return result;
        }

        int endNode = end;
        String route = "";
        while(endNode != start) {
            route =  ", " + connectedTo[endNode] + route;
            endNode = connectedTo[endNode];
        }
        route = route + ", " + end;

        result.add("cost: " + Double.toString(distanceTo[end]));
        result.add(" via route: ");
        result.add(route);
        return result;
    }

    private void getShorterRoute(double[] distanceTo, int[] connectedTo, int start, int end) {
        if(distanceTo[end] - distanceTo[start] > busStopMatrix[start][end]) {
            distanceTo[end] = distanceTo[start] + busStopMatrix[start][end];
            connectedTo[end] = start;
        }
    }
}

